package com.pranav.dubey.assistance;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SOSsetup extends AppCompatActivity {
String mEmail;
    EditText contact;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sossetup);

        mEmail=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL);
        contact= (EditText) findViewById(R.id.sosContactNumber);
        textView= (TextView) findViewById(R.id.SOSprevNumber);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(mEmail, 0); // 0 - for private mode
        if (pref.getBoolean("loggedin",false)){
            String contactNumber=pref.getString("contact","No Number Set");
            textView.setText("Previous Contact Number For SOS: "+contactNumber);
        }


    }
    public void saveSOSButtonClicked(View view){
        String contactNumber=contact.getText().toString().trim();
        if (TextUtils.isEmpty(contactNumber)){
            contact.setError("This field cannot be empty");
            contact.requestFocus();
        }else if (contactNumber.length()<10){
            contact.setError("Enter a valid 10 digit mobile number");
            contact.requestFocus();
        }else if (contactNumber.length()>12){
            contact.setError("Who are you trying to send SOS to");
            contact.requestFocus();
        }else{
            SharedPreferences pref = getApplicationContext().getSharedPreferences(mEmail, 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("loggedin",true);
            editor.putString("contact", contactNumber);//Enter Default Number Here
            editor.commit();
            if (pref.getBoolean("loggedin",false)){
                String contactNumb=pref.getString("contact","No Number Set");
                textView.setText("Previous Contact Number For SOS: "+contactNumb);
            }
        }

    }
}
