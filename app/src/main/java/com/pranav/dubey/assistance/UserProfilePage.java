package com.pranav.dubey.assistance;

import com.pranav.dubey.assistance.UserDatabaseContract.UserDetails;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfilePage extends AppCompatActivity {
    TextView mNameView, mEmailView, mMobileView;
    public String mFirstName, mMiddleName, mLastName, mFullName, mEmail, mMobileNumber, mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_page);
        mNameView = (TextView) findViewById(R.id.name);
        mEmailView = (TextView) findViewById(R.id.email_add);
        mMobileView = (TextView) findViewById(R.id.mobileNumber);

        mFirstName = getIntent().getStringExtra(UserDetails.COLUMN_NAME_FIRST_NAME);
        mMiddleName = getIntent().getStringExtra(UserDetails.COLUMN_NAME_MIDDLE_NAME);
        mLastName = getIntent().getStringExtra(UserDetails.COLUMN_NAME_LAST_NAME);
        mEmail = getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL);
        mMobileNumber = getIntent().getStringExtra(UserDetails.COLUMN_NAME_MOBILE_NUMBER);
        mPassword = getIntent().getStringExtra(UserDetails.COLUMN_NAME_PASSWORD);
        if (TextUtils.isEmpty(mMiddleName)) {
            mFullName = mFirstName + " " + mLastName;
        } else {
            mFullName = mFirstName + " " + mMiddleName + " " + mLastName;
        }
        mNameView.setText(mFullName);
        mEmailView.setText(mEmail);
        mMobileView.setText(mMobileNumber);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(mEmail, 0); // 0 - for private mode
        if (!(pref.getBoolean("loggedin", false))) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("loggedin", true);
            editor.putString("contact", "9559438524");//Enter Default Number Here
            editor.commit();
        }
    }

    public void logOut(View view) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
        SharedPreferences pr = getApplicationContext().getSharedPreferences(mEmail, 0);
        SharedPreferences.Editor editor1 = pr.edit();
        editor1.clear();
        editor1.commit();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void onEditClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), EditUserData.class);
        intent.putExtra(UserDetails.COLUMN_NAME_FIRST_NAME, mFirstName);
        intent.putExtra(UserDetails.COLUMN_NAME_MIDDLE_NAME, mMiddleName);
        intent.putExtra(UserDetails.COLUMN_NAME_LAST_NAME, mLastName);
        intent.putExtra(UserDetails.COLUMN_NAME_MOBILE_NUMBER, mMobileNumber);
        intent.putExtra(UserDetails.COLUMN_NAME_EMAIL, mEmail);
        intent.putExtra(UserDetails.COLUMN_NAME_PASSWORD, mPassword);
        startActivity(intent);
        finish();
    }

    public void deactivateClicked(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert")
                .setMessage("You are about to deactivate your account.All your information will be deleted.Are you sure?")
                .setPositiveButton("I KNOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DeactivateAccount mDeactTask = new DeactivateAccount(mEmail);
                        mDeactTask.execute((Void[]) null);
                    }
                })
                .setNegativeButton("TAKE ME BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do nothing
                    }
                }).show();
    }

    public class DeactivateAccount extends AsyncTask<Void, Void, Boolean> {
        private final String mEmail;

        DeactivateAccount(String email) {
            mEmail = email;
        }

        UserDatabaseDbHelper mDbHelper = new UserDatabaseDbHelper(getApplicationContext());

        @Override
        protected Boolean doInBackground(Void... voids) {
            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            db.delete(UserDetails.TABLE_NAME, UserDetails.COLUMN_NAME_EMAIL + " = ?",
                    new String[]{String.valueOf(mEmail)});
            db.close();
            mDbHelper.close();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
            SharedPreferences pr = getApplicationContext().getSharedPreferences(mEmail, 0);
            SharedPreferences.Editor editor1 = pr.edit();
            editor1.clear();
            editor1.commit();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    public void SOSsetup(View view) {
        Intent intent = new Intent(getApplicationContext(), SOSsetup.class);
        intent.putExtra(UserDetails.COLUMN_NAME_EMAIL, mEmail);
        startActivity(intent);

    }

    public void SOSClicked(View view) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(mEmail, 0); // 0 - for private mode
        String phoneNo = pref.getString("contact", "9559438524");
        String msg = "Help Me I'm In This Taxi";//Here Deatils of taxi will be given
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

    }

    public void CallCustomerCare(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "9559438524"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
           Toast.makeText(getApplicationContext(),"Please Allow This app To make calls From App Settings",Toast.LENGTH_LONG).show();
            return;
        }
        startActivity(callIntent);
    }
}
